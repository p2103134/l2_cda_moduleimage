#include "ImageViewer.h"
#include <iostream>

ImageViewer::ImageViewer() {

    // initialisation SDL2
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Erreur lors de l'initialisation de SDL" << std::endl;
        return;
    }

    // création de la fenêtre
    window = SDL_CreateWindow(
        "Visionneuse d'image",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        200,
        200,
        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
    );
    if (window == nullptr) {
        std::cerr << "Erreur lors de la création de la fenêtre" << std::endl;
        SDL_Quit();
        return;
    }
    SDL_SetWindowResizable(window, SDL_FALSE); // empeche le redimensionnement de la fenêtre

    // création du renderer
    renderer = SDL_CreateRenderer(
        window,
        -1,
        SDL_RENDERER_ACCELERATED
    );
    if (renderer == nullptr) {
        std::cerr << "Erreur lors de la création du renderer" << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
        return;
    }
}

ImageViewer::~ImageViewer() {
    // on désalloue quand plus besoin
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void ImageViewer::renderImage(const Image& im) {
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
    SDL_RenderClear(renderer);

    int zoomWidth = im.getWidth() * zoomLevel;
    int zoomHeight = im.getHeight() * zoomLevel;

    SDL_Rect dstRect = { 100 - (zoomWidth / 2), 100 - (zoomHeight / 2), zoomWidth, zoomHeight };
    SDL_RenderCopy(renderer, texture, nullptr, &dstRect);

    SDL_RenderPresent(renderer);
}

void ImageViewer::afficher(const Image& im) {
    // vérifier que l'image est affichable
    if (im.getWidth() <= 0 || im.getHeight() <= 0 || im.getTab() == nullptr) {
        std::cerr << "Image invalide" << std::endl;
        return;
    }

    // création d'une surface SDL à partir du tableau de pixels
    surface = SDL_CreateRGBSurfaceFrom(
        (void*)im.getTab(),
        im.getWidth(),
        im.getHeight(),
        24,
        im.getWidth() * 3,
        0x0000FF,
        0x00FF00,
        0xFF0000,
        0
    );
    if (surface == nullptr) {
        std::cerr << "Erreur lors de la création de la surface" << std::endl;
        return;
    }

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (texture == nullptr) {
        std::cerr << "Erreur lors de la création de la texture" << std::endl;
        SDL_FreeSurface(surface);
        return;
    }
    SDL_FreeSurface(surface);

    zoomLevel = 1;
    renderImage(im);

    bool quit = false;
    SDL_Event event;
    while (!quit) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            } else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    case SDLK_t:
                        zoomLevel++;
                        renderImage(im);
                        break;
                    case SDLK_g:
                        if (zoomLevel > 1)
                            zoomLevel--;
                        renderImage(im);
                        break;
                }
            }
        }
        // cap à 60 FPS (1000ms / 60 ~= 16)
        SDL_Delay(16);
    }

    SDL_DestroyTexture(texture);
}