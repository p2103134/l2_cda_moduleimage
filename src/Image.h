#ifndef _IMAGE_H
#define _IMAGE_H

#include <string>
#include "Pixel.h"

/**
 * @class Image
 * 
 * @brief Réprésente une image
 */
class Image {

    private:
        Pixel* tab;
        int dimx, dimy;

    public:

        /**
         * @brief Instancie une nouvelle image de 0x0 pixels
         * @return Image
         */
        Image();

        /**
         * @brief Instancie une nouvelle image de dimx * dimy pixels
         * 
         * @param dimx
         * @param dimy
         * @return Image
         */
        Image(int dimx, int dimy);

        /**
         * @brief Désalloue le tableau de pixels en mémoire
         */
        ~Image();

        /**
         * @brief Retourne la largeur de l'image
         * 
         * @return int 
         */
        int getWidth() const;

        /**
         * @brief Retourne la hauteur de l'image
         * 
         * @return int 
         */
        int getHeight() const;

        /**
         * @brief Retourne le pointeur sur tableau de pixels
         * 
         * @return Pixel*
         */
        Pixel* getTab() const;

        /**
         * @brief Récupère une référence du pixel original (x,y)
         * 
         * @param x 
         * @param y 
         * @return Pixel& 
         */
        Pixel& getPix(int x, int y);

        /**
         * @brief Récupère une copie du pixel (x,y)
         * 
         * @param x 
         * @param y 
         * @return Pixel 
         */
        Pixel getPix(int x, int y) const;

        /**
         * @brief Modifie le pixel de coordonnées (x,y)
         * 
         * @param x 
         * @param y 
         * @param couleur 
         */
        void setPix(int x, int y, const Pixel &couleur);

        /**
         * @brief Dessine un rectangle pleins de couleur 
         * 
         * @param Xmin 
         * @param Ymin 
         * @param Xmax 
         * @param Ymax 
         * @param couleur 
         */
        void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel &couleur);
        
        /**
         * @brief Efface l'image en la remplissant de couleur
         * 
         * @param couleur 
         */
        void effacer(const Pixel &couleur);

        /**
         * @brief Sauvegarde l'image dans un fichier passé en paramètre 
         * 
         * @param filename 
         */
        void sauver(const std::string &filename) const;

        /**
         * @brief Ouvre l'image passé en paramètre
         * 
         * @param filename 
         */
        void ouvrir(const std::string &filename);

        /**
         * @brief Affiche l'image de manière numérique en console
         * 
         */
        void afficherConsole();

        /**
         * @brief Procédure permettant d'effectuer tous les tests de régression de Image
         */
        static void testRegression();
};

#endif