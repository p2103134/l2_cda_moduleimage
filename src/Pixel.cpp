#include "Pixel.h"

Pixel::Pixel() : r(0), g(0), b(0) {}

Pixel::Pixel(unsigned char rr, unsigned char gg, unsigned char bb) : r(rr), g(gg), b(bb) {}