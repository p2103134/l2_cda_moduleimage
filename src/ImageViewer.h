#ifndef _IMAGEVIEWER_H
#define _IMAGEVIEWER_H

#include "SDL2/SDL.h"
#include "Image.h"

/**
 * @class ImageViewer
 * 
 * @brief Represente une visionneuse d'image
 */
class ImageViewer {
    
    private:
        SDL_Window* window = nullptr;
        SDL_Renderer* renderer = nullptr;
        SDL_Surface* surface = nullptr;
        SDL_Texture* texture = nullptr;
        int zoomLevel = 1;
        void renderImage(const Image& im);

    public:
        /**
         * @brief Instancie une visionneuse d'image
         * @return ImageViewer
         */
        ImageViewer();

        /**
         * @brief Désalloue la visionneuse d'image en mémoire
         */
        ~ImageViewer();

        /**
         * @brief Affiche une Image dans le visionneuse d'image
         * 
         * @param im 
         */
        void afficher(const Image& im);

};

#endif