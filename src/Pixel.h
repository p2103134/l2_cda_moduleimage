#ifndef _PIXEL_H
#define _PIXEL_H

/**
 * @class Pixel
 * 
 * @brief Réprésente un pixel
 */
struct Pixel {

    unsigned char r; ///< Quantité rouge
    unsigned char g; ///< Quantité vert
    unsigned char b; ///< Quantité bleu

    /**
     * @brief Instancie un nouveau pixel noir
     * @return Pixel
     */
    Pixel();
    
    /**
     * @brief Instancie un nouveau pixel avec couleur passée en paramètres
     * 
     * @param nr 
     * @param ng 
     * @param nb 
     * 
     * @return Pixel
     */
    Pixel(unsigned char nr, unsigned char ng, unsigned char nb);

    void afficherPixel() const;
    
};

#endif