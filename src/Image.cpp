#include "Image.h"
#include <cassert>
#include <iostream>
#include <fstream>

Image::Image() {
    dimx = 0;
    dimy = 0;
    tab = nullptr;
}

Image::Image(int dimx, int dimy) {
    this->dimx = dimx;
    this->dimy = dimy;
    tab = new Pixel[dimx*dimy];
}

Image::~Image() {
    if(tab != nullptr) {
        delete [] tab;
        tab = nullptr;
    }
    dimx = 0;
    dimy = 0;
}

int Image::getWidth() const { return dimx; }
int Image::getHeight() const { return dimy; }
Pixel* Image::getTab() const { return tab; }

Pixel& Image::getPix(int x, int y) {
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    return tab[y * dimx + x];
}

Pixel Image::getPix(int x, int y) const {
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    return tab[y * dimx + x];
}

void Image::setPix(int x, int y, const Pixel &couleur) {
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    getPix(x, y) = couleur;
}

void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel &couleur) {
    assert(Xmin >= 0 && Xmin < dimx && Ymin >= 0 && Xmin < dimy);
    assert(Xmax >= Xmin && Ymax >= Ymin && Xmax < dimx && Ymax < dimy);
    for(int i = Xmin; i <= Xmax; i++) {
        for(int j = Ymin; j <= Ymax; j++) {
            setPix(i, j, couleur);
        }
    }
}

void Image::effacer(const Pixel &couleur) {
    dessinerRectangle(0, 0, dimx-1, dimy-1, couleur);
}

void Image::sauver(const std::string &filename) const
{
    std::ofstream fichier(filename.c_str());
    assert(fichier.is_open());

    fichier << "P3" << std::endl;
    fichier << dimx << " " << dimy << std::endl;
    fichier << "255" << std::endl;

    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << std::endl;
        }
    std::cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const std::string &filename)
{
    std::ifstream fichier(filename.c_str());
    assert(fichier.is_open());

    int r, g, b;
    std::string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);

    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];

    for(int y = 0; y < dimy; y++) {
        for(int x = 0; x < dimx; x++) {
            fichier >> r >> g >> b;
            Pixel& currentPix = getPix(x,y);
            currentPix.r = char(r);
            currentPix.g = char(g);
            currentPix.b = char(b);
        }
    }

    fichier.close();
    std::cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    std::cout << dimx << " " << dimy << std::endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            std::cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        std::cout << std::endl;
    }
}

void Image::testRegression() {
    // test création Image par défaut
    Image img;
    assert(img.dimx == 0 && img.dimy == 0);
    assert(img.tab == nullptr);

    // test création Image avec dimensions
    Image img2(10,10);
    assert(img2.dimx == 10 && img2.dimy == 10);
    assert(img2.tab != nullptr);

    // test récupération et modification d'un pixel
    Image img3(10,10);
    Pixel& p = img3.getPix(5,5);
    assert(p.r == 0 && p.g == 0 && p.b == 0);
    img3.setPix(5,5, Pixel(255,255,255));
    assert(p.r == 255 && p.g == 255 && p.b == 255);

    // test dessin rectangle
    Image img5(10,10);
    Pixel couleur(255,0,0);
    img5.dessinerRectangle(0,0,5,5,couleur);
    for(int i = 0; i < 5; i++) {
        for(int j = 0; j < 5; j++) {
            Pixel& currentPixel = img5.getPix(i,j);
            assert(currentPixel.r == couleur.r 
                && currentPixel.g == couleur.g
                && currentPixel.b == couleur.b);
        }
    }
}