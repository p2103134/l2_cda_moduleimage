# L2_CDA_ModuleImage

Projet réalisé par Smail YAHIOUNE (P2103134), id forge: 33042

## Compilation du projet

### Sur Linux

- Installez les dépendences et outils nécessaires, sur Ubuntu 22.04 par exemple :
```
sudo apt update -y
sudo apt install build-essential cmake doxygen libsdl2-dev -y
```

- Initialiser les fichiers de compilation avec CMake puis compiler :
```
cd build
cmake ..
make
```

Les fichiers executables sont placés à la racine du projet dans ``bin/``

- Pour générer la documentation du projet (depuis le dossier build/), faites :
```
make doc 
```

### Sur Windows

- Vous aurez besoin de Visual Studio avec le module C++ d'installé, de CMake et d'SDL2 présent sur la machine (installé via VCPKG par exemple)
- Vous pouvez aussi compiler votre propre version d'SDL2 en passant l'option **VENDORED** à **ON** dans CMake et en mettant le code source d'SDL2 dans un dossier ``vendored/sdl`` à la racine du projet
- Ouvrez le projet dans CMake GUI, selectionnez la racine du projet dans source code et le dossier ``build/`` après
- Faites **Generate** et choisissez votre version de Visual Studio, normalement **SDL2_DIR** contient le chemin vers le SDL2 (précisement les scripts CMake de SDL) installé sur votre système, sinon compilez votre propre version comme expliqué plus haut
- Faites **Open Project** le projet s'ouvre dans VC puis CTRL + SHIFT + B pour construire la cible **ALL_BUILD**
- Les fichiers executables au format **.exe** seront placés dans ``bin/Debug``
- Attention à bien garder le **SDL2d.dll** dans le même dossier utilisé lors de la compilation ou sinon le mettre dans le dossier courant du **affichage.exe**
- Lancez ``cmake --build . --target doc`` depuis ``build/`` pour générer la documentation (Doxygen doit être installé sur la machine)

## Explication

Chaque executable peut être lancé par la CLI ou un double clic:

- ``bin/exemple`` doit être executé avec un dossier ``data/`` de présent, il générera une image au format PPM puis l'ouvrira pour la modifier pour créer une deuxième image

- ``bin/test`` execute une série de test de régression sur le module **Image**

- ``bin/affichage`` ouvre une fenêtre grâce à la bibliothèque SDL2 (liée dynamiquement au programme), cette fenêtre est de 200x200 non redimenssionable avec un fond gris clair par défaut, c'est une visionneuse d'image qui permet de visualiser une image codé grâce à la classe **Image**, l'image est par défaut affiché en taille réelle mais on peut zoomer et dezoomer dessus avec les touches **T** et **G**, on peut quitter le programme avec la touche Echape. Le rendu de l'image a été cappé à 60 fois par seconde pour éviter de consommer inutilement trop de ressources CPU.